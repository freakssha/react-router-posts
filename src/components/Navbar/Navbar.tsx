import React from 'react';
import {NavLink, useLocation} from "react-router-dom";
import "./Navbar.css"

interface INavbar {
    isAuth: boolean,
    changeAuth: () => void,
}

const Navbar = ({isAuth, changeAuth}: INavbar) => {
    const location = useLocation();

    return (
        <header className="App-header">
            <NavLink style={{color: location.pathname === "/" ? "yellow" : "white"}} to="/">главная</NavLink>
            <NavLink style={{color: location.pathname === "/add-post" ? "yellow" : "white"}} to="/add-post">создать
                пост</NavLink>
            <NavLink style={{color: location.pathname.slice(0, 6) === "/users" ? "yellow" : "white"}}
                     to="/users">пользователи</NavLink>
            <NavLink style={{color: location.pathname.slice(0, 6) === "/posts" ? "yellow" : "white"}}
                     to="/posts">посты</NavLink>
            <NavLink style={{color: location.pathname.slice(0, 9) === "/products" ? "yellow" : "white"}}
                     to="/products">продукты</NavLink>
            <button onClick={changeAuth}>
                {
                    isAuth ? "разлогиниться" : "залогиниться"
                }
            </button>
        </header>
    );
}

export default Navbar;
