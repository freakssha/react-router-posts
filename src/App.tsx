import React, {useState} from 'react';
import './App.css';
import {Redirect, Route, Switch} from "react-router-dom";
import Posts from "./views/Posts";
import Post from "./views/Post";
import AddPost from "./views/AddPost";
import Navbar from "./components/Navbar/Navbar";
import Error404 from "./views/Error404";
import Home from "./views/Home";
import Users from "./views/Users";
import User from "./views/User";
import Products from "./views/Products";
import Product from "./views/Product";

function App() {
    const [isAuth, setAuth] = useState(false);

    return (
        <div className="App">
            <Navbar isAuth={isAuth} changeAuth={() => setAuth(isAuth => !isAuth)}/>
            <Switch>
                <Route path="/" component={Home} exact/>
                <Route path="/posts" component={Posts} exact/>
                <Route path="/posts/:id" component={Post}/>
                <Route path="/users" component={Users} exact/>
                <Route path="/users/:id" component={User}/>
                <Route path="/products" component={Products} exact/>
                <Route path="/products/:id" component={Product}/>
                <Route path="/add-post">
                    {isAuth ? <AddPost/> : <Redirect to="/"/>}
                </Route>
                <Route component={Error404}/>
            </Switch>
        </div>
    );
}

export default App;
