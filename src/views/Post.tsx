import React from 'react';
import {useParams} from "react-router-dom";
import {posts} from "../data";


const Post = () => {
    const {id} = useParams<{ id: string }>()
    const post: any = posts.filter(post => (':' + post.id == id))[0]

    return (
        <div>
            <h1>Страница поста</h1>
            <h3>{post.title}</h3>
        </div>
    );
}

export default Post;
