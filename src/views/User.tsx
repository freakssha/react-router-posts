import React from 'react';
import {useParams} from "react-router-dom";
import {users} from "../data";


const User = () => {
    const {id} = useParams<{ id: string }>()
    const user: any = users.filter(user => (':' + user.id == id))[0]

    return (
        <div>
            <h1>Страница пользователя</h1>
            <h3>{user.title}</h3>
        </div>
    );
}

export default User;
