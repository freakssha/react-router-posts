import React from 'react';
import {useParams} from "react-router-dom";
import {products} from "../data";


const Product = () => {
    const {id} = useParams<{ id: string }>()
    const product: any = products.filter(product => (':' + product.id == id))[0]

    return (
        <div>
            <h1>Страница продукта</h1>
            <h3>{product.title}</h3>
        </div>
    );
}

export default Product;
