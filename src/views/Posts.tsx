import React from 'react';
import {posts} from "../data";
import {Link} from "react-router-dom";

function Posts() {
    return (
        <div className="Posts">
            {
                posts.map(post => (
                    <Link key={post.id} to={`/posts/:${post.id}`}>
                        <h3>{post.title}</h3>
                    </Link>
                ))
            }
        </div>
    );
}

export default Posts;
