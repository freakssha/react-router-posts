import React from 'react';
import {products} from "../data";
import {Link} from "react-router-dom";

function Products() {
    return (
        <div className="Products">
            {
                products.map(product => (
                    <Link key={product.id} to={`/products/:${product.id}`}>
                        <h3>{product.title}</h3>
                    </Link>
                ))
            }
        </div>
    );
}

export default Products;
