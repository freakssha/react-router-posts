import React from 'react';
import {users} from "../data";
import {Link} from "react-router-dom";

function Users() {
    return (
        <div className="Users">
            {
                users.map(user => (
                    <Link key={user.id} to={`/users/:${user.id}`}>
                        <h3>{user.title}</h3>
                    </Link>
                ))
            }
        </div>
    );
}

export default Users;
