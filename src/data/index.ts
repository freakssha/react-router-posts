export const posts = [
    {
        id: Math.random(),
        title: "Я пост на русском языке for node-sass (https://github.com/sass/node-sass).",
    },
    {
        id: Math.random(),
        title: "Пример хорошего поведения",
    },
    {
        id: Math.random(),
        title: "Пост - организация личной деятельности",
    },
]

export const users = [
    {
        id: Math.random(),
        title: "Masha",
    },
    {
        id: Math.random(),
        title: "Maria",
    },
    {
        id: Math.random(),
        title: "Vasya",
    },
]

export const products = [
    {
        id: Math.random(),
        title: "Cat",
    },
    {
        id: Math.random(),
        title: "Apple",
    },
    {
        id: Math.random(),
        title: "Cirque",
    },
]